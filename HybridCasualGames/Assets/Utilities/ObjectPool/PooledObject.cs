using UnityEngine;

public interface IPooledObject
{
    void OnObjectSpawn();
}

public class PooledObject : MonoBehaviour, IPooledObject
{
    public void OnObjectSpawn()
    {
        // Implement custom initialization or behavior here
    }
}
