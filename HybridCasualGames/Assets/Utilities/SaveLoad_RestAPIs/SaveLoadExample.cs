using UnityEngine;

[System.Serializable]
public class ExampleCustomClass
{
    public string exampleVariable;
    // Add more fields as needed
}

public class SaveLoadExample : MonoBehaviour
{
    public void SavePlayerData(ExampleCustomClass example)
    {
        RestSaveSystem.Instance.SendPostRequest("https://your-api-endpoint.com/save", example, OnSaveResponse);
    }

    public void LoadPlayerData()
    {
        RestSaveSystem.Instance.SendGetRequest("https://your-api-endpoint.com/load", OnLoadResponse);
    }

    private void OnSaveResponse(object responseData)
    {
        // Check if the response data is not null
        if (responseData != null)
        {
            // Assuming the server responds with a message indicating the save was successful
            string message = responseData.ToString();
            Debug.Log("Save response: " + message);
        }
        else
        {
            Debug.LogError("Save response data is null");
        }
    }

    private void OnLoadResponse(object responseData)
    {
        // Check if the response data is not null
        if (responseData != null)
        {
            // Assuming the server responds with the loaded player data
            ExampleCustomClass example = responseData as ExampleCustomClass;
            if (example != null)
            {
                // Do something with the loaded player data
                Debug.Log("Loaded player data: " + example.exampleVariable);
            }
            else
            {
                Debug.LogError("Failed to deserialize loaded player data");
            }
        }
        else
        {
            Debug.LogError("Load response data is null");
        }
    }
}
