using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;

public class RestSaveSystem : Singleton<RestSaveSystem>
{
    public void SendPostRequest(string url, object data, Action<object> callback)
    {
        StartCoroutine(SendPostRequestCoroutine(url, data, callback));
    }

    public void SendGetRequest(string url, Action<object> callback)
    {
        StartCoroutine(SendGetRequestCoroutine(url, callback));
    }

    private IEnumerator SendPostRequestCoroutine(string url, object data, Action<object> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("data", JsonUtility.ToJson(data));

        UnityWebRequest www = UnityWebRequest.Post(url, form);
        yield return www.SendWebRequest();

        HandleResponse(www, callback);
    }

    private IEnumerator SendGetRequestCoroutine(string url, Action<object> callback)
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        HandleResponse(www, callback);
    }

    private void HandleResponse(UnityWebRequest www, Action<object> callback)
    {
        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("Error: " + www.error);
        }
        else
        {
            string responseData = www.downloadHandler.text;
            object deserializedData = JsonUtility.FromJson(responseData, typeof(object)); // Replace 'object' with the appropriate type
            callback?.Invoke(deserializedData);
        }
    }

}
