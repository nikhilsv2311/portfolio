public class SingletonExample : Singleton<SingletonExample>
{
    //This script is now a singleton. This can be referred using SingletonExample.Instance
}
