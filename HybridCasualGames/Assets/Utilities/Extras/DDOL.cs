public class DDOL : Singleton<DDOL>
{
    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(this);
    }

    // Method to destroy the GameObject if needed
    public void DestroyIfNeeded()
    {
        Destroy(gameObject);
    }
}
