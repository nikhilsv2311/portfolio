using UnityEngine;

public enum Languages
{
    English,
    French
}

[CreateAssetMenu(fileName = "ScriptableObjectLocalization", menuName = "Localization/Scriptable Object Localization", order = 1)]
public class ScriptableObjectLocalization : ScriptableObject
{
    public Languages language;
    public TextAsset localizationFile;
    public LocalizationData localizationData;
    
    public void ExtractJsonToCustomClass()
    {
        if (localizationFile != null)
        {
            string jsonString = localizationFile.text;
            localizationData = JsonManager.Instance.LoadFromJson<LocalizationData>(localizationFile);
        }
    }
}


