using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LocalizationManager : Singleton<LocalizationManager>
{
    [SerializeField] private List<ScriptableObjectLocalization> scriptableObjectLocalizations = new List<ScriptableObjectLocalization>();
    private Dictionary<Languages, LocalizationData> localizedTextDictionary = new Dictionary<Languages, LocalizationData>();

    [Header("UI Elements affected by Language")]
    [SerializeField] private TextMeshProUGUI greetings;
    protected override void Awake()
    {
        base.Awake();
        LoadLocalizedText();
        SetLanguage(Languages.English);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            SetLanguage(Languages.English);
        }
        else if (Input.GetKeyDown(KeyCode.F))
        {
            SetLanguage(Languages.French);
        }
    }
    /// <summary>
    /// Creates a Dictionary by extracting the data from the List of ScriptableObjectLocalizations
    /// </summary>
    void LoadLocalizedText()
    {
        if(scriptableObjectLocalizations.Count >0)
        {
            foreach(var localization in scriptableObjectLocalizations)
            {
                localization.ExtractJsonToCustomClass();
                localizedTextDictionary.Add(localization.language, localization.localizationData);
            }
        }
    }
    /// <summary>
    /// Returns the LocalizationData(Custom Data Class) for the given language
    /// </summary>
    /// <param name="language"></param>
    /// <returns></returns>
    public LocalizationData GetLocalizedValue(Languages language)
    {
        if (localizedTextDictionary.ContainsKey(language))
            return localizedTextDictionary[language];
        else
            return null; // Key not found in dictionary
    }

    /// <summary>
    /// Sets the langauge of the game and changes the text contents 
    /// </summary>
    /// <param name="language"></param>
    public void SetLanguage(Languages language)
    {
        LocalizationData data =  GetLocalizedValue(language);
        // Optionally update UI elements, text, audio, and other content with new language
        greetings.text = data.greeting;
    }

    public void SetLanguageOnButtonClick(LanguageSetter languageSetter)
    {
        SetLanguage(languageSetter.language);
    }
}


/// <summary>
/// Custom Data Class , change this according to the JSON string in the TextAsset
/// </summary>
[System.Serializable]
public class LocalizationData
{
    public string greeting;
    public string farewell;
    public string welcome_message;
}
