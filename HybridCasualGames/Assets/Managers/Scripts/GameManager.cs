using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] Sound bgm;
    void Start()
    {
        AudioManager.Instance.InitializeSound(bgm);
        AudioManager.Instance.PlayBGM(bgm);
    }
}
