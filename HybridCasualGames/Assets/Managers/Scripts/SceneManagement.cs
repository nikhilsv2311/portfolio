using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[Serializable]
public class ProgressBar
{
    // Reference to the UI Slider component
    public Slider slider;

    // Set the value of the progress bar
    public void SetValue(float value)
    {
        if (slider != null)
            slider.value = value;
    }
}

public class SceneManagement : Singleton<SceneManagement>
{
    // Optional: Reference to a loading screen UI element
    public GameObject loadingScreen;
    // Optional: Reference to a loading progress UI element
    public ProgressBar progressBar = new ProgressBar();

    /// <summary>
    /// Use this method when you want to load a specific scene by its name. This is useful for cases where you know the name of the scene you want to load.
    /// </summary>
    /// <param name="sceneName"></param>
    public static void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    /// <summary>
    /// Similar to the previous method, but instead of providing the scene name, you provide the build index of the scene. This can be useful if you want to load a scene based on its position in the build settings.
    /// </summary>
    /// <param name="sceneBuildIndex"></param>
    public static void LoadScene(int sceneBuildIndex)
    {
        SceneManager.LoadScene(sceneBuildIndex);
    }

    /// <summary>
    /// Use this method when you want to reload the current active scene. This is useful for implementing features like restarting a level or reloading the game after certain events.
    /// </summary>
    public static void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    /// <summary>
    /// Use this method when you want to load the next scene in the build order. This can be useful for implementing level progression systems or advancing to the next level after completing a current one.
    /// </summary>
    public static void LoadNextScene()
    {
        int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
        if (nextSceneIndex < SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(nextSceneIndex);
        }
        else
        {
            Debug.LogWarning("No next scene available.");
        }
    }

    /// <summary>
    /// Use this method when you want to quit the application. This can be useful for implementing a quit button or providing an option for users to exit the game.
    /// </summary>
    public static void Quit()
    {
        Application.Quit();
    }

    /// <summary>
    /// Use this method when you want to load a scene with a transition effect and a loading screen. This is useful for providing visual feedback to the player during scene transitions.
    /// </summary>
    /// <param name="sceneName"></param>
    public void LoadSceneWithTransition(string sceneName)
    {
        StartCoroutine(LoadSceneCoroutine(sceneName));
    }

    // Coroutine for loading a scene asynchronously with a loading screen
    private IEnumerator LoadSceneCoroutine(string sceneName)
    {
        ShowLoadingScreen();

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        asyncLoad.allowSceneActivation = false;

        while (!asyncLoad.isDone)
        {
            if (progressBar != null)
            {
                progressBar.SetValue(asyncLoad.progress);
                ShowLoadingProgress(asyncLoad.progress);
            }

            // If loading is almost complete, allow scene activation
            if (asyncLoad.progress >= 0.9f)
            {
                asyncLoad.allowSceneActivation = true;
            }

            yield return null;
        }

        HideLoadingScreen();
    }

    // Show loading screen
    public void ShowLoadingScreen()
    {
        if (loadingScreen != null)
            loadingScreen.SetActive(true);
    }

    // Hide loading screen
    public void HideLoadingScreen()
    {
        if (loadingScreen != null)
            loadingScreen.SetActive(false);
    }

    /// <summary>
    /// Use this method to update the progress of the loading screen. This can be useful for displaying a progress bar or percentage indicator to inform the player about the progress of loading.
    /// </summary>
    /// <param name="progress"></param>
    public void ShowLoadingProgress(float progress)
    {
        if (progressBar != null)
            progressBar.SetValue(progress);
    }
}
