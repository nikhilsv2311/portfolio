using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuManager : Singleton<MenuManager>
{
    [SerializeField] private GameObject[] menus; // Array of menu GameObjects
    [SerializeField] private float transitionDuration = 0.5f; // Duration of menu transition animation

    private GameObject currentMenu; // Reference to the current active menu

    // Start is called before the first frame update
    void Start()
    {
       Init();
    }

    void Init()
    {
        // Hide all menus except the first one
        foreach (GameObject menu in menus)
        {
            menu.SetActive(false);
        }
        if (menus.Length > 0)
        {
            ShowMenu(menus[0]);
        }
    }

    // Show a specific menu by name
    public void ShowMenu(string menuName)
    {
        foreach (GameObject menu in menus)
        {
            if (menu.name == menuName)
            {
                ShowMenu(menu);
                return;
            }
        }
        Debug.LogWarning("Menu '" + menuName + "' not found.");
    }

    // Show a specific menu and hide the rest
    public void ShowMenu(GameObject menu)
    {
        if (currentMenu != null)
        {
            // Fade out the current menu
            CanvasGroup currentCanvasGroup = currentMenu.GetComponent<CanvasGroup>();
            if (currentCanvasGroup != null)
            {
                StartCoroutine(FadeCanvasGroup(currentCanvasGroup, currentCanvasGroup.alpha, 0f, transitionDuration));
            }
        }

        // Show the new menu after the transition duration
        StartCoroutine(ShowMenuAfterDelay(menu, transitionDuration));
    }

    private IEnumerator ShowMenuAfterDelay(GameObject menu, float delay)
    {
        yield return new WaitForSeconds(delay);

        if (currentMenu != null)
        {
            currentMenu.SetActive(false);
        }
        menu.SetActive(true);

        // Fade in the new menu
        CanvasGroup menuCanvasGroup = menu.GetComponent<CanvasGroup>();
        if (menuCanvasGroup != null)
        {
            StartCoroutine(FadeCanvasGroup(menuCanvasGroup, menuCanvasGroup.alpha, 1f, transitionDuration));
        }

        currentMenu = menu;
    }


    // Coroutine to fade a CanvasGroup from one alpha value to another over time
    private IEnumerator FadeCanvasGroup(CanvasGroup canvasGroup, float startAlpha, float endAlpha, float duration)
    {
        float elapsedTime = 0f;
        while (elapsedTime < duration)
        {
            elapsedTime += Time.deltaTime;
            canvasGroup.alpha = Mathf.Lerp(startAlpha, endAlpha, elapsedTime / duration);
            yield return null;
        }
        canvasGroup.alpha = endAlpha;
    }
}
