using UnityEngine;

public class JsonManager : Singleton<JsonManager>
{
    /// <summary>
    /// Extracts the JSON from the TextAsset into the Custom class T 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="jsonAsset"></param>
    /// <returns></returns>
    public T LoadFromJson<T>(TextAsset jsonAsset)
    {
        if (jsonAsset == null)
        {
            Debug.LogError("JSON asset is null.");
            return default;
        }

        T data;

        try
        {
            data = JsonUtility.FromJson<T>(jsonAsset.text);
        }
        catch (System.Exception e)
        {
            Debug.LogError("Error deserializing JSON:\n" + e.Message);
            return default;
        }

        return data;
    }

    /// <summary>
    /// Converts the Custom class T data into a JSON string and store it in a file at the given filePath
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="data"></param>
    /// <param name="filePath"></param>
    public void SaveToJson<T>(T data, string filePath)
    {
        try
        {
            string jsonString = JsonUtility.ToJson(data, prettyPrint: true);
            System.IO.File.WriteAllText(filePath, jsonString);
        }
        catch (System.Exception e)
        {
            Debug.LogError("Error serializing JSON:\n" + e.Message);
        }
    }
}
