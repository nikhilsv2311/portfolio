using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;
    public float followSpeed = 5f;
    public float rotationSpeed = 5f;
    public float zoomSpeed = 5f;
    public float minZoomDistance = 2f;
    public float maxZoomDistance = 10f;
    public float orbitSensitivity = 1f;
    public float topDownHeight = 10f;

    private Vector3 offset;
    private Quaternion initialRotation;
    private float initialDistance;

    void Start()
    {
        offset = transform.position - target.position;
        initialRotation = transform.rotation;
        initialDistance = offset.magnitude;
    }

    void Update()
    {
        HandleInput();
    }

    void FollowTarget()
    {
        if (target == null)
            return;

        Vector3 desiredPosition = target.position + offset;
        Quaternion desiredRotation = Quaternion.LookRotation(target.position - transform.position, Vector3.up);

        transform.position = Vector3.Lerp(transform.position, desiredPosition, followSpeed * Time.deltaTime);
        transform.rotation = Quaternion.Slerp(transform.rotation, desiredRotation, rotationSpeed * Time.deltaTime);
    }

    void OrbitTarget()
    {
        if (target == null)
            return;

        if (Input.GetMouseButton(1)) // Right mouse button
        {
            float horizontalInput = Input.GetAxis("Mouse X");
            float verticalInput = Input.GetAxis("Mouse Y");

            transform.RotateAround(target.position, Vector3.up, horizontalInput * orbitSensitivity);
            transform.RotateAround(target.position, transform.right, -verticalInput * orbitSensitivity);
        }
    }

    void TopDownView()
    {
        if (target == null)
            return;

        Vector3 topDownPosition = target.position + Vector3.up * topDownHeight;
        Quaternion topDownRotation = Quaternion.Euler(90f, 0f, 0f);

        transform.position = topDownPosition;
        transform.rotation = topDownRotation;
    }

    void Zoom()
    {
        float scrollInput = Input.GetAxis("Mouse ScrollWheel");
        float zoomDistance = offset.magnitude - scrollInput * zoomSpeed;
        zoomDistance = Mathf.Clamp(zoomDistance, minZoomDistance, maxZoomDistance);
        offset = offset.normalized * zoomDistance;
    }

    void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.F)) // Switch to follow mode
        {
            transform.rotation = initialRotation;
            offset = transform.position - target.position;
        }

        if (Input.GetKeyDown(KeyCode.O)) // Switch to orbit mode
        {
            offset = transform.position - target.position;
        }

        if (Input.GetKeyDown(KeyCode.T)) // Switch to top-down mode
        {
            TopDownView();
        }

        if (Input.GetMouseButton(2)) // Middle mouse button for panning
        {
            float horizontalInput = Input.GetAxis("Mouse X");
            float verticalInput = Input.GetAxis("Mouse Y");

            transform.Translate(-horizontalInput, -verticalInput, 0f);
        }

        OrbitTarget();
        Zoom();
    }

    void LateUpdate()
    {
        if (Input.GetKey(KeyCode.LeftShift)) // Smooth follow when holding Left Shift
        {
            FollowTarget();
        }
    }
}
