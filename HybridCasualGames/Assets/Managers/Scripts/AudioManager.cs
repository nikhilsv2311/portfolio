using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

// Sound class to store audio data
[System.Serializable]
public class Sound
{
    public string name;
    public AudioClip clip;
    [Range(0f, 1f)] public float volume = 1f;
    [Range(0.1f, 3f)] public float pitch = 1f;
    public bool loop = false;

    public AudioSource source;
}
public class AudioManager : Singleton<AudioManager> 
{
    // Array of Sound objects
    public Sound[] sounds;

    // Initialize the AudioManager
    void Start()
    {
        // Assign AudioClip to AudioSources
        for (int i = 0; i < sounds.Length; i++)
        {
            InitializeSound(sounds[i]);
        }
    }

    public void InitializeSound(Sound sound)
    {
        sound.source.clip = sound.clip;
        sound.source.volume = sound.volume;
        sound.source.pitch = sound.pitch;
        sound.source.loop = sound.loop;
    }
    // Method to play a sound by name
    public void Play(string soundName)
    {
        Sound s = FindSound(soundName);
        if (s != null)
        {
            s.source.PlayOneShot(s.clip);
        }
        else
        {
            Debug.LogWarning("Sound with name " + soundName + " not found!");
        }
    }

    // Method to find a Sound object by name
    private Sound FindSound(string soundName)
    {
        foreach (Sound s in sounds)
        {
            if (s.name == soundName)
            {
                return s;
            }
        }
        return null;
    }

    // Method to play background music
    public void PlayBGM(Sound bgm)
    {
        InitializeSound(bgm);
        if (bgm != null && bgm.source != null && !bgm.source.isPlaying)
        {
            bgm.source.Play();
        }
    }

    // Method to stop background music
    public void StopBGM(Sound bgm)
    {
        if (bgm != null && bgm.source != null && bgm.source.isPlaying)
        {
            bgm.source.Stop();
        }
    }

    // Method to play background music with fade-in effect
    public void PlayBGMWithFadeIn(Sound bgm, float fadeInDuration)
    {
        InitializeSound(bgm);
        if (bgm != null && bgm.source != null && !bgm.source.isPlaying)
        {
            bgm.source.volume = 0f;
            bgm.source.Play();
            StartCoroutine(FadeInBGM(bgm, fadeInDuration));
        }
    }

    // Coroutine for fade-in effect
    private IEnumerator FadeInBGM(Sound bgm, float fadeInDuration)
    {
        float timer = 0f;
        while (timer < fadeInDuration)
        {
            timer += Time.deltaTime;
            bgm.source.volume = Mathf.Lerp(0f, bgm.volume, timer / fadeInDuration);
            yield return null;
        }
        bgm.source.volume = bgm.volume;
    }
    // Method to play background music with audio mixing and ducking
    public void PlayBGMWithMixingAndDucking(Sound bgm, float duckingVolume, float duckingDuration)
    {
        InitializeSound(bgm);
        if (bgm != null && bgm.source != null && !bgm.source.isPlaying)
        {
            foreach (Sound sound in sounds)
            {
                if (sound != bgm && sound.source.isPlaying)
                {
                    StartCoroutine(DuckSound(sound, duckingVolume, duckingDuration));
                }
            }
            bgm.source.Play();
        }
    }

    // Coroutine for ducking effect
    private IEnumerator DuckSound(Sound sound, float duckingVolume, float duckingDuration)
    {
        float initialVolume = sound.source.volume;
        float timer = 0f;
        while (timer < duckingDuration)
        {
            timer += Time.deltaTime;
            sound.source.volume = Mathf.Lerp(initialVolume, duckingVolume, timer / duckingDuration);
            yield return null;
        }
        sound.source.volume = duckingVolume;
    }
}
