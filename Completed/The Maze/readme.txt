The Maze

The game objective is to collect the 13 food cubes and reach the end of the maze. 
The path to the end goes through a sequence of gates to be opened in a particular order. 
The gates are colored and the respective colored cube needs to be acquired in order to open the gate.
--------------------------------------------------------------------------------------------------------
I. Folder/File List

Maze_Data 
Maze 
Maze.exe 
--------------------------------------------------------------------------------------------------------
II. Prerequisites

System requirements
OS: Windows 7 SP1+, 8, 10, 64-bit versions only
GPU: Graphics card with DX9 (shader model 2.0) capabilities. Anything made since 2004 should work.

Software required 
Unity3D (version 4.x and above)

Download Unity3D

https://unity3d.com/get-unity/download
--------------------------------------------------------------------------------------------------------
III. Issues

While collecting the blue and orange cubes be careful in the order that they are collected. It might end 
up in a stalemate.

There is no ESC option once the game starts. 

These will be addressed in the next version along with new levels. 
--------------------------------------------------------------------------------------------------------
IV. Acknowledgement

Unity3D tutorials
Youtube tutors
--------------------------------------------------------------------------------------------------------


