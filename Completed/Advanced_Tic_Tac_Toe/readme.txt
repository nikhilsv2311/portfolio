Advanced Tic-Tac-Toe

A normal Tic-Tac-Toe has a 3x3 matrix as the game space.
The Advanced Tic-Tac-Toe has 3x3 smaller matrixes inside every cell of the normal matrix. 
In order to claim a cell of the normal matrix, the player has to win the smaller Tic-Tac-Toe present in that cell.
------------------------------------------------------------------------------------------------------------------
I. Folder/File List

Debug  				Advanced_Tic_Tac_Toe.vcxproj			Advanced_Tic_Tac_Toe.vcxproj.user
Adv_tic_tac_toe.cpp		Advanced_Tic_Tac_Toe.vcxproj.filters 
		
------------------------------------------------------------------------------------------------------------------
II. Prerequisites

System requirements
OS: Windows 7 SP1+, 8, 10, 64-bit versions only


Software required 
Visual Studio Community (version 2015 and above)

Download Visual Studio Community

https://www.visualstudio.com/downloads/
--------------------------------------------------------------------------------------------------------
III. Issues

The game currently runs on the console of Visual Studios.

The small bugs will be addressed in the coming versions along with inclusion of graphics.
--------------------------------------------------------------------------------------------------------
IV. Acknowledgement

Reference books on C++
Varun P Rao (Friend) 
--------------------------------------------------------------------------------------------------------


